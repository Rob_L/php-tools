<?php

	date_default_timezone_set('UTC');

class crypto{
	function enc_encrypt($string, $key) {
		$result = '';
		for($i = 0; $i < strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char) + ord($keychar));
			$result .= $char;
		}

		return base64_encode($result);
	}

	function enc_decrypt($string, $key) {
		$result = '';
		$string = base64_decode($string);

		for($i = 0; $i < strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char) - ord($keychar));
			$result .= $char;
		}

		return $result;
	}


	function rotate($str, $pass){
		$pass = str_split(str_pad('', strlen($str), $pass, STR_PAD_RIGHT));
		$stra = str_split($str);
		foreach($stra as $k=>$v){
			$tmp = ord($v)+ord($pass[$k]);
			$stra[$k] = chr( $tmp > 255 ?($tmp-256):$tmp);
		}
		return join('', $stra);
    }

    function straighten($str, $pass){
		$pass = str_split(str_pad('', strlen($str), $pass, STR_PAD_RIGHT));
		$stra = str_split($str);
		foreach($stra as $k=>$v){
			$tmp = ord($v)-ord($pass[$k]);
			$stra[$k] = chr( $tmp < 0 ?($tmp+256):$tmp);
		}
		return join('', $stra);
    }

}

class time_tools{

	public $date_time;

    public function __construct(){
		$this->date_time = date("Y-m-d H:i:s");
    }
	public function Lapsed_Time_Human($time){

		$time = strtotime($this->date_time) - strtotime($time); // to get the time since that moment

		$tokens = array (
		    31536000 => 'year',
		    2592000 => 'month',
		    604800 => 'week',
		    86400 => 'day',
		    3600 => 'hour',
		    60 => 'minute',
		    1 => 'second'
		);

		foreach ($tokens as $unit => $text) {
		    if ($time < $unit) continue;
		    $numberOfUnits = floor($time / $unit);
		    return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
		}

	}
/*
$d1 = new DateTime('2008-08-03 14:52:10');
$d2 = new DateTime('2008-01-03 11:11:10');
var_dump($d1 == $d2);
var_dump($d1 > $d2);
var_dump($d1 < $d2);

$date1 = new DateTime("now");
$date2 = new DateTime("tomorrow");
*/	
}
class client_info{

	public function get_client_ip_remote() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

}

		

/*********************
 * 	tools() class  ***
 * *******************
 * 
 * This class is the general interface by which the tools are used and should be the only class
 * on this page that gets called from scripts running outside of this page.
 * 
 */	class tools{


    public function MD5_From_Array($arr){// deprecating this in favor of Checksum()
    	return $this->Checksum($arr);
    }
    
    public function IPAddress(){	
		$client = new client_info();
		return $client->get_client_ip_remote();
	}

	public function Checksum($param){
		if(is_array($param)){
			return md5(serialize($param));
		}else{
			return md5($param);
		}
	}

	public function Human_Lapsed_Time($time){
		$time_tool = new time_tools();
		return $time_tool->Lapsed_Time_Human($time);
	}
	
	public function Short_Lapsed_Time($time){
		$time_tool = new time_tools();
		return $time_tool->Lapsed_Time_Short($time);
	}		

}
?>
